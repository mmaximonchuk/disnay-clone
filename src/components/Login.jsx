import React from 'react';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import imgBackground from '../assets/images/login-background.jpg';
import imgCTA1 from '../assets/images/cta-logo-one.svg';
import imgCTA2 from '../assets/images/cta-logo-two.png';
function Login() {
  return (
    <Container bgImage={imgBackground}>
      <CTA>
        <CTALogoOne src={imgCTA1} />
        <SignUp to="/">Get All There</SignUp>
        <Description>
          ​Get Premier Access to Raya and the Last Dragon for an additional fee with a Disney+
          subscription. As of 03/26/21, the price of Disney+ and The Disney Bundle will increase by
          $1.
        </Description>
        <CTALogoTwo src={imgCTA2} />
      </CTA>
    </Container>
  );
}

export default Login;

const Container = styled.div`
  min-height: calc(100vh - 70px);
  padding: 0 calc(3.5vw + 5px);
  position: relative;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  &::before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background-image: ${(props) => `url(${props.bgImage})`};
    background-size: cover;
    background-repeat: no-repeat;
    background-position: top;
    z-index: -1;
    opacity: 0.7;
  }
`;
const CTA = styled.div`
  max-width: 650px;
  padding: 80px 40px;
  margin-top: 10vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const CTALogoOne = styled.img``;
const CTALogoTwo = styled.img`
  width: 90%;
`;
const SignUp = styled(Link)`
  text-transform: uppercase;
  width: 100%;
  background-color: #0063e5;
  padding: 17px 0;
  font-weight: bold;
  color: #f9f9f9;
  border-radius: 4px;
  text-align: center;
  font-size: 18px;
  cursor: pointer;
  transition: all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
  letter-spacing: 1.5px;
  margin-top: 8px;
  margin-bottom: 12px;
  &:hover {
    background: #0483ee;
  }
`;
const Description = styled.p`
  font-size: 11px;
  line-height: 1.5;
  letter-spacing: 1.5px;
  text-align: center;
`;
