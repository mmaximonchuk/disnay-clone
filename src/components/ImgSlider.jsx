import React from 'react';
import styled from 'styled-components';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import imgSlider1 from '../assets/images/slider-badging.jpg';
import imgSlider2 from '../assets/images/slider-badag.jpg';
import imgSlider3 from '../assets/images/slider-scale.jpg';
import imgSlider4 from '../assets/images/slider-scales.jpg';

function ImgSlider() {
    const settings = {
        dots: true,
        arrows: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
    };

    return (
        <Carousel {...settings}>
            <Wrap>
                <img src={imgSlider1} />
            </Wrap>
            <Wrap>
                <img src={imgSlider2} />
            </Wrap>
            <Wrap>
                <img src={imgSlider3} />
            </Wrap>
            <Wrap>
                <img src={imgSlider4} />
            </Wrap>
        </Carousel>
    );
}

export default ImgSlider;

const Carousel = styled(Slider)`
    margin-top: 20px;
    .slick-list {
        overflow: visible;
    }
    ul li button {
        &::before {
            font-size: 10px;
            color: rgb(150, 158, 171);
        }
    }
    li.slick-active button::before {
        color: white;
    }
    button {
        z-index: 1;
    }
`;

const Wrap = styled.div`
    cursor: pointer;
    img {
        border: 4px solid transparent;
        border-radius: 4px;
        object-fit: cover;
        object-position: center;
        width: 100%;
        height: 100%;
        min-height: 179px;
        box-shadow: rgb(0 0 0 / 69%) 0px 26px 30px -10px, rgb(0 0 0 / 73%) 0px 16px 10px -10px;
        transition: border 0.3s;
        &:hover {
            border: 4px solid rgba(249, 249, 249, 0.8);
        }
    }
`;
