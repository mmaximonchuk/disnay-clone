import React from 'react';
import styled from 'styled-components';
import imgDisney from '../assets/images/viewers-disney.png';
import imgMarvel from '../assets/images/viewers-marvel.png';
import imgNational from '../assets/images/viewers-national.png';
import imgPixar from '../assets/images/viewers-pixar.png';
import imgStarWars from '../assets/images/viewers-starwars.png';
import videoDisney from '../assets/videos/1564674844-disney.mp4';
import videoMarvel from '../assets/videos/1564676115-marvel.mp4';
import videoNational from '../assets/videos/1564676296-national-geographic.mp4';
import videoPixar from '../assets/videos/1564676714-pixar.mp4';
import videoStarWars from '../assets/videos/1608229455-star-wars.mp4';

function Viewers() {
  const handleMouseOver = (event) => {
    if (event.target?.childNodes[1]) {
      event.target.childNodes[1].play();
    }
  };
  const handleTouch = (touchevent) => {
    const touchTarget = touchevent.target.closest('div');
    touchTarget.childNodes[1].play();
  };
  
  return (
    <Container>
      <Wrap onMouseOver={handleMouseOver} onTouchStart={handleTouch}>
        <img src={imgDisney} alt="" />
        <video loop="" muted="" autoPlay="" poster="video/plane.jpg" className="bg__video">
          <source src={videoDisney} type="video/mp4" />
        </video>
      </Wrap>
      <Wrap onMouseOver={handleMouseOver} onTouchStart={handleTouch}>
        <img src={imgMarvel} alt="" />
        <video loop="" muted="" autoPlay="" poster="video/plane.jpg" className="bg__video">
          <source src={videoMarvel} type="video/mp4" />
        </video>
      </Wrap>
      <Wrap onMouseOver={handleMouseOver} onTouchStart={handleTouch}>
        <img src={imgNational} alt="" />
        <video loop="" muted="" autoPlay="" poster="video/plane.jpg" className="bg__video">
          <source src={videoNational} type="video/mp4" />
        </video>
      </Wrap>
      <Wrap onMouseOver={handleMouseOver} onTouchStart={handleTouch}>
        <img src={imgPixar} alt="" />
        <video loop="" muted="" autoPlay="" poster="video/plane.jpg" className="bg__video">
          <source src={videoPixar} type="video/mp4" />
        </video>
      </Wrap>
      <Wrap onMouseOver={handleMouseOver} onTouchStart={handleTouch}>
        <img src={imgStarWars} alt="" />
        <video loop="" muted="" autoPlay="" poster="video/plane.jpg" className="bg__video">
          <source src={videoStarWars} type="video/mp4" />
        </video>
      </Wrap>
    </Container>
  );
}

export default Viewers;

const Container = styled.div`
  padding: 30px 0 26px;
  margin-top: 30px;
  display: grid;
  grid-template-columns: repeat(5, minmax(0, 1fr));
  grid-gap: 25px;
  @media (max-width: 910px) {
    grid-template-columns: repeat(3, minmax(0, 1fr));
    grid-gap: 15px;
  }
  @media (max-width: 575px) {
    grid-template-columns: repeat(2, minmax(0, 1fr));
    grid-gap: 15px;
  }
  @media (max-width: 475px) {
    grid-template-columns: repeat(1, minmax(0, 1fr));
    grid-gap: 15px;
  }
`;
const Wrap = styled.div`
  border: 3px solid rgba(249, 249, 249, 0.1);
  border-radius: 10px;
  box-shadow: rgb(0 0 0 / 69%) 0px 26px 30px -10px, rgb(0 0 0 / 73%) 0px 16px 10px -10px;
  transition: all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
  position: relative;
  cursor: pointer;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  .bg__video {
    opacity: 0;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    border-radius: 10px;
    transition: all 0.3s;
  }
  &:hover {
    box-shadow: rgb(0 0 0 / 80%) 0px 40px 58px -16px, rgb(0 0 0 / 73%) 0px 30px 22px -10px;
    transform: scale(1.05);
    border-color: rgba(249, 249, 249, 0.8);
    .bg__video {
      opacity: 1;
    }
  }
`;
