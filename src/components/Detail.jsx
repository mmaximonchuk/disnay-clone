import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import styled, { css } from 'styled-components';
import iconPlay from '../assets/images/play-icon-black.png';
import iconTrailer from '../assets/images/play-icon-white.png';
import iconGroup from '../assets/images/group-icon.png';
import db from '../firebase';
import { useDispatch } from 'react-redux';
function Detail(props) {
  const [movie, setMovie] = useState(null);
  const { id } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    (async () => {
      const result = await db.collection('movies').doc(id).get();
      if (result.exists) {
        setMovie(result.data());
      } else {
      }
    })();
  }, []);

  return (
    <Container>
      {movie ? (
        <>
          <Background>
            <img src={movie.backgroundImg} alt={movie.title} />
          </Background>
          <ImageTitle>
            <img src={movie.titleImg} alt={movie.title} />
          </ImageTitle>
          <Controls>
            <PlayButton>
              <img src={iconPlay} alt="Play" />
              <span>Play</span>
            </PlayButton>
            <TrailerButton>
              <img src={iconTrailer} alt="Trailer" />
              <span>Trailer</span>
            </TrailerButton>
            <AddButton>
              <span>+</span>
            </AddButton>
            <GroupWatchButton>
              <img src={iconGroup} alt="iconGroup" />
            </GroupWatchButton>
          </Controls>

          <SubTitle>{movie.subTitle}</SubTitle>
          <Description>{movie.description}</Description>
        </>
      ) : (
        <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
      )}
    </Container>
  );
}

export default Detail;

const Container = styled.div`
  min-height: calc(100vh - 200px);
  padding: 0 calc(3.5vw + 5px);
  position: relative;

  .lds-ellipsis {
    display: inline-block;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 154px;
    height: 120px;
  }
  .lds-ellipsis div {
    position: absolute;
    top: 33px;
    width: 26px;
    height: 26px;
    border-radius: 50%;
    background: #fff;
    animation-timing-function: cubic-bezier(0, 1, 1, 0);
  }
  .lds-ellipsis div:nth-child(1) {
    left: 0px;
    animation: lds-ellipsis1 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(2) {
    left: 0px;
    animation: lds-ellipsis2 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(3) {
    left: 64px;
    animation: lds-ellipsis2 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(4) {
    left: 128px;
    animation: lds-ellipsis3 0.6s infinite;
  }
  @keyframes lds-ellipsis1 {
    0% {
      transform: scale(0);
    }
    100% {
      transform: scale(1);
    }
  }
  @keyframes lds-ellipsis3 {
    0% {
      transform: scale(1);
    }
    100% {
      transform: scale(0);
    }
  }
  @keyframes lds-ellipsis2 {
    0% {
      transform: translate(0, 0);
    }
    100% {
      transform: translate(64px, 0);
    }
  }
`;
const Background = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: -1;
  opacity: 0.8;
  img {
    object-fit: cover;
    object-position: center;
    width: 100%;
    height: 100%;
  }
`;
const ImageTitle = styled.div`
  height: 30vh;
  min-height: 170px;
  width: 35vw;
  min-width: 200px;
  margin: 3vw 0;
  img {
    object-fit: contain;
    width: 100%;
    height: 100%;
  }
`;

const Controls = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  @media (max-width: 575px) {
    flex-wrap: wrap;
  }
  @media (max-width: 375px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

const buttonStyles = css`
  border-radius: 4px;
  font-size: 15px;
  display: flex;
  align-items: center;
  padding: 0 24px;
  border: none;
  letter-spacing: 1.8px;
  cursor: pointer;
  height: 56px;
  text-transform: uppercase;
  margin-right: 24px;
  transition: all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
`;

const PlayButton = styled.button`
  ${buttonStyles}
  background-color: rgb(249, 249, 249);
  &:hover {
    background-color: rgb(198, 198, 198);
  }
  @media (max-width: 575px) {
    margin-bottom: 15px;
  }
`;
const TrailerButton = styled.button`
  ${buttonStyles}
  background: rgba(0,0,0, .3);
  border: 1px solid rgb(249, 249, 249);
  color: rgb(249, 249, 249);
  &:hover {
    background-color: rgb(198, 198, 198);
    color: rgb(0, 0, 0);
  }
  @media (max-width: 575px) {
    margin-bottom: 15px;
  }
`;
const AddButton = styled.button`
  ${buttonStyles}
  padding: 0;
  width: 44px;
  height: 44px;
  display: flex;
  align-items: center;
  background: rgba(0, 0, 0, 0.6);
  border: 2px solid rgb(249, 249, 249);
  justify-content: center;
  border-radius: 50%;
  margin-right: 16px;
  span {
    font-size: 30px;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
    letter-spacing: 1px;
  }
  &:hover {
    background-color: rgb(198, 198, 198);
    border: 2px solid rgba(0, 0, 0, 0.6);
  }
  @media (max-width: 375px) {
    margin-bottom: 15px;
  }
`;
const GroupWatchButton = styled(AddButton)`
  background-color: rgb(0, 0, 0);
  @media (max-width: 375px) {
    position: absolute;
    bottom: 0;
    left: 60px;
  }
`;
const SubTitle = styled.p`
  color: rgb(249, 249, 249);
  font-size: 15px;
  min-height: 20px;
  margin-top: 26px;
`;
const Description = styled.p`
  line-height: 1.4;
  font-size: 20px;
  margin-top: 16px;
  max-width: 760px;
  color: rgb(249, 249, 249);
`;
