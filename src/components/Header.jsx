import React, { useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { selectUserName, selectUserPhoto } from '../features/user/userSlice';
import { useDispatch, useSelector } from 'react-redux';
import { auth, provider } from '../firebase';
import { setUserLogIn, setSignOut } from '../features/user/userSlice';
import styled from 'styled-components';
import imgLogo from '../assets/images/logo.svg';
import imgHome from '../assets/images/home-icon.svg';
import imgSearch from '../assets/images/search-icon.svg';
import imgWatchList from '../assets/images/watchlist-icon.svg';
import imgOriginals from '../assets/images/original-icon.svg';
import imgMovies from '../assets/images/movie-icon.svg';
import imgSeries from '../assets/images/series-icon.svg';
import iconUserProfile from '../assets/images/user-img.jpg';

function Header() {
  const history = useHistory();
  const dispatch = useDispatch();
  const userName = useSelector(selectUserName);
  const userPhoto = useSelector(selectUserPhoto);

  useEffect(() => {
    auth.onAuthStateChanged(async (user) => {
      if (user) {
        dispatch(
          setUserLogIn({
            name: user.displayName,
            email: user.email,
            photo: user.photoURL,
          }),
        );
        history.push('/');
      }
    });
  }, []);

  const signIn = async () => {
    const result = await auth.signInWithPopup(provider);
    dispatch(
      setUserLogIn({
        name: result.user.displayName,
        email: result.user.email,
        photo: result.user.photoURL,
      }),
    );
    history.push('/');
  };

  const signOut = async () => {
    await auth.signOut();
    dispatch(setSignOut());
    history.push('/login');
  };

  return (
    <Nav>
      <Link to="/">
        <Logo src={imgLogo} />
      </Link>
      {!userName ? (
        <Login onClick={signIn}>Login</Login>
      ) : (
        <>
          <NavMenu>
            <a href="">
              <img src={imgHome} alt="home link" />
              <span>Home</span>
            </a>
            <a href="">
              <img src={imgSearch} alt="home link" />
              <span>Search</span>
            </a>
            <a href="">
              <img src={imgWatchList} alt="home link" />
              <span>WatchList</span>
            </a>
            <a href="">
              <img src={imgOriginals} alt="home link" />
              <span>Originals</span>
            </a>
            <a href="">
              <img src={imgMovies} alt="home link" />
              <span>Movies</span>
            </a>
            <a href="">
              <img src={imgSeries} alt="home link" />
              <span>Series</span>
            </a>
          </NavMenu>

          <UserImg src={userPhoto || iconUserProfile} onClick={signOut} />
        </>
      )}
    </Nav>
  );
}

export default Header;

const Nav = styled.nav`
  height: 70px;
  background-color: #090b13;
  color: white;
  display: flex;
  align-items: center;
  padding: 0 36px;
  overflow-x: hidden;
`;
const Logo = styled.img`
  width: 80px;
`;

const NavMenu = styled.div`
  display: flex;
  flex: 1;
  margin-left: 20px;
  align-items: center;
  a {
    display: flex;
    align-items: center;
    padding: 0 12px;
    &:hover span::after {
      width: 100%;
    }
    img {
      height: 20px;
      margin-right: 7px;
    }
    span {
      text-transform: uppercase;
      font-size: 13px;
      letter-spacing: 1.42px;
      position: relative;
      &::after {
        content: '';
        position: absolute;
        bottom: -5px;
        left: -1px;
        height: 2px;
        transition: all 0.3s;
        background-color: white;
        width: 0;
      }
    }
  }
  @media(max-width: 910px) {
     a {
        display: none;
     } 
  }
`;

const UserImg = styled.img`
  border-radius: 50%;
  object-fit: cover;
  object-position: center;
  width: 40px;
  height: 40px;
  cursor: pointer;
`;
const Login = styled.button`
  border: 1px solid #f9f9f9;
  padding: 8px 16px;
  background-color: rgba(0, 0, 0, 0.6);
  color: #f9f9f9;
  border-radius: 4px;
  letter-spacing: 1.5px;
  transition: all 0.25s;
  cursor: pointer;
  text-transform: uppercase;
  margin-left: auto;
  &:hover {
    color: rgba(0, 0, 0, 0.6);
    background-color: #f9f9f9;
    border: 1px solid transparent;
  }
`;
